import { Marketplace } from "./../typechain-types/contracts/Marketplace";
import { Token } from "./../typechain-types/contracts/Token";
import { NFT721 } from "./../typechain-types/contracts/NFT721";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Marketplace Test", function () {
  let deployer, alice, bob, daniel: any;
  let nft721: NFT721;
  let token: Token;
  let marketplace: Marketplace;
  this.beforeEach(async () => {
    [deployer, alice, bob, daniel] = await ethers.getSigners();
    nft721 = (await (
      await (await ethers.getContractFactory("NFT721")).deploy()
    ).deployed()) as NFT721;
    token = (await (
      await (await ethers.getContractFactory("Token")).deploy()
    ).deployed()) as Token;
    marketplace = (await (
      await (
        await ethers.getContractFactory("Marketplace")
      ).deploy(token.address, nft721.address)
    ).deployed()) as Marketplace;
    await nft721.mintBatch(deployer.address, 100);
    await nft721.mintBatch(alice.address, 100);
    await nft721.mintBatch(bob.address, 100);
    await token.mint(ethers.utils.parseEther("10000"), alice.address);
    await token.mint(ethers.utils.parseEther("10000"), bob.address);
    await token.mint(ethers.utils.parseEther("10000"), daniel.address);
  });

  it("List token on Marketplace", async () => {
    // alice list her nft to marketplace
    await nft721.connect(alice).approve(marketplace.address, 101);
    await marketplace
      .connect(alice)
      .listItem(101, ethers.utils.parseEther("1000"));
    const listItem = await marketplace.connect(alice).getMyListedItem();
    const itemId = await marketplace._marketIdtoId(listItem[0]);
    expect(itemId).to.equal(101);
    const marketplaceBalance = await nft721.balanceOf(marketplace.address);
    expect(marketplaceBalance).to.equal(1);
  });

  it("List many tokens on Marketplace", async () => {
    await nft721.connect(alice).approve(marketplace.address, 101);
    await marketplace
      .connect(alice)
      .listItem(101, ethers.utils.parseEther("1000"));
    await nft721.connect(bob).approve(marketplace.address, 201);
    await marketplace
      .connect(bob)
      .listItem(201, ethers.utils.parseEther("2000"));
    const listItem = await marketplace.getListedItem();
    console.log("list item", listItem);
    const aliceItemId = await marketplace._marketIdtoId(listItem[0]);
    const bobItemId = await marketplace._marketIdtoId(listItem[1]);
    expect(aliceItemId).to.equal(101);
    expect(bobItemId).to.equal(201);
    const marketplaceBalance = await nft721.balanceOf(marketplace.address);
    expect(marketplaceBalance).to.equal(2);
  });

  it("List, delist and list again", async () => {
    await nft721.connect(alice).approve(marketplace.address, 101);
    await marketplace
      .connect(alice)
      .listItem(101, ethers.utils.parseEther("1000"));
    await marketplace.connect(alice).delistItem(1);
    await nft721.connect(alice).approve(marketplace.address, 101);
    await marketplace
      .connect(alice)
      .listItem(101, ethers.utils.parseEther("1000"));
    const listItem = await marketplace.connect(alice).getMyListedItem();
    expect(listItem[0]).to.equal(2);
  });

  it("Buy item", async () => {
    await nft721.connect(alice).approve(marketplace.address, 101);
    await marketplace
      .connect(alice)
      .listItem(101, ethers.utils.parseEther("1000"));
    // daniel will buy nft from alice via marketplace
    await token
      .connect(daniel)
      .approve(marketplace.address, ethers.utils.parseEther("1000"));
    await marketplace.connect(daniel).buyItem(1);
    const aliceTokenBalance = await token.balanceOf(alice.address);
    const danielTokenBalance = await token.balanceOf(daniel.address);
    expect(aliceTokenBalance).to.equal(ethers.utils.parseEther("11000"));
    expect(danielTokenBalance).to.equal(ethers.utils.parseEther("9000"));
    const danielNFTTokenBalance = await nft721.balanceOf(daniel.address);
    expect(danielNFTTokenBalance).to.equal(1);
  });
});
