import { NFT721 } from "./../typechain-types/contracts/NFT721";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Demo NFT721", function () {
  let deployer, alice, bob, daniel: any;
  let nft721: NFT721;
  beforeEach(async () => {
    [deployer, alice, bob, daniel] = await ethers.getSigners();
    nft721 = (await (
      await (await ethers.getContractFactory("NFT721")).deploy()
    ).deployed()) as NFT721;
  });

  it("Test single mint", async () => {
    await nft721.mint(alice.address);
    const aliceBalance = await nft721.balanceOf(alice.address);
    console.log(aliceBalance, "balance");
    expect(aliceBalance).to.equal(1);
  });

  it("Test batch mint", async () => {
    await nft721.mintBatch(bob.address, 100);
    const bobBalance = await nft721.balanceOf(bob.address);
    expect(bobBalance).to.equal(100);
  });
});
