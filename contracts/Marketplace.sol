// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.4;
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract Marketplace is Ownable, IERC721Receiver {
    // variables and mappings
    using Counters for Counters.Counter;
    Counters.Counter private _marketIds;
    address public tokenAddress;
    address public nftAddress;

    mapping(uint256 => uint256) public _mappingPrice;
    mapping(uint256 => address) public _owner;
    mapping(uint256 => uint256) public _marketIdtoId;

    // structs and events
    event ListItem(address lister, uint256 itemId, uint256 price);
    event DelistItem(address owner, uint256 itemId);
    event BuyItem(address buyer, uint256 itemId);

    constructor(address _tokenAddress, address _nftAddress) {
        tokenAddress = _tokenAddress;
        nftAddress = _nftAddress;
    }

    function setTokenAddress(address _tokenAddress) external onlyOwner {
        tokenAddress = _tokenAddress;
    }

    function setNftAddress(address _nftAddress) external onlyOwner {
        nftAddress = _nftAddress;
    }

    function listItem(uint256 _itemId, uint256 _price) public {
        _listItem(_itemId, _price);
    }

    function listBatchItem(uint256[] memory _itemIds, uint256[] memory _prices)
        public
        onlyOwner
    {
        for (uint256 i = 0; i < _itemIds.length; i++) {
            _listItem(_itemIds[i], _prices[i]);
        }
    }

    function delistItem(uint256 _marketId) public {
        IERC721 nft = IERC721(nftAddress);
        uint256 tokenId = _marketIdtoId[_marketId];
        require(tokenId != 0, "invalid market id");
        nft.transferFrom(address(this), msg.sender, tokenId);
        emit DelistItem(msg.sender, tokenId);
        delete _owner[_marketId];
        delete _mappingPrice[_marketId];
        delete _marketIdtoId[_marketId];
    }

    function buyItem(uint256 _marketId) public {
        IERC721 nft = IERC721(nftAddress);
        IERC20 token = IERC20(tokenAddress);

        uint256 tokenId = _marketIdtoId[_marketId];
        uint256 price = _mappingPrice[_marketId];
        address owner = _owner[_marketId];

        token.transferFrom(msg.sender, owner, price);
        nft.transferFrom(address(this), msg.sender, tokenId);
        emit BuyItem(msg.sender, tokenId);
        delete _owner[_marketId];
        delete _mappingPrice[_marketId];
        delete _marketIdtoId[_marketId];
    }

    function getListedItem() public view returns (uint256[] memory) {
        uint256 currentMarketId = _marketIds.current();
        uint256 amount = 0;
        uint256 count = 0;
        for (uint256 i = 0; i <= currentMarketId; i++) {
            if (_marketIdtoId[i] != 0) {
                amount++;
            }
        }
        uint256[] memory listedItem = new uint256[](amount);
        for (uint256 i = 0; i <= currentMarketId; i++) {
            if (_marketIdtoId[i] != 0) {
                listedItem[count] = i;
                count++;
            }
        }
        return listedItem;
    }

    function getMyListedItem() public view returns (uint256[] memory) {
        uint256 currentMarketId = _marketIds.current();
        uint256 amount = 0;
        uint256 count = 0;
        for (uint256 i = 0; i <= currentMarketId; i++) {
            if (_marketIdtoId[i] != 0 && _owner[i] == msg.sender) {
                amount++;
            }
        }
        uint256[] memory listedItem = new uint256[](amount);
        for (uint256 i = 0; i <= currentMarketId; i++) {
            if (_marketIdtoId[i] != 0 && _owner[i] == msg.sender) {
                listedItem[count] = i;
                count++;
            }
        }
        return listedItem;
    }

    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) external pure override returns (bytes4) {
        return this.onERC721Received.selector;
    }

    // internal functions
    function _listItem(uint256 _itemId, uint256 _price) internal {
        _marketIds.increment();
        IERC721 nft = IERC721(nftAddress);

        nft.transferFrom(msg.sender, address(this), _itemId);
        _owner[_marketIds.current()] = msg.sender;
        _mappingPrice[_marketIds.current()] = _price;
        _marketIdtoId[_marketIds.current()] = _itemId;

        emit ListItem(msg.sender, _itemId, _price);
    }
}
