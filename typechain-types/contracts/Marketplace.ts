/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  BaseContract,
  BigNumber,
  BigNumberish,
  BytesLike,
  CallOverrides,
  ContractTransaction,
  Overrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers";
import type {
  FunctionFragment,
  Result,
  EventFragment,
} from "@ethersproject/abi";
import type { Listener, Provider } from "@ethersproject/providers";
import type {
  TypedEventFilter,
  TypedEvent,
  TypedListener,
  OnEvent,
} from "../common";

export interface MarketplaceInterface extends utils.Interface {
  functions: {
    "_mappingPrice(uint256)": FunctionFragment;
    "_marketIdtoId(uint256)": FunctionFragment;
    "_owner(uint256)": FunctionFragment;
    "buyItem(uint256)": FunctionFragment;
    "delistItem(uint256)": FunctionFragment;
    "getListedItem()": FunctionFragment;
    "getMyListedItem()": FunctionFragment;
    "listBatchItem(uint256[],uint256[])": FunctionFragment;
    "listItem(uint256,uint256)": FunctionFragment;
    "nftAddress()": FunctionFragment;
    "onERC721Received(address,address,uint256,bytes)": FunctionFragment;
    "owner()": FunctionFragment;
    "renounceOwnership()": FunctionFragment;
    "setNftAddress(address)": FunctionFragment;
    "setTokenAddress(address)": FunctionFragment;
    "tokenAddress()": FunctionFragment;
    "transferOwnership(address)": FunctionFragment;
  };

  getFunction(
    nameOrSignatureOrTopic:
      | "_mappingPrice"
      | "_marketIdtoId"
      | "_owner"
      | "buyItem"
      | "delistItem"
      | "getListedItem"
      | "getMyListedItem"
      | "listBatchItem"
      | "listItem"
      | "nftAddress"
      | "onERC721Received"
      | "owner"
      | "renounceOwnership"
      | "setNftAddress"
      | "setTokenAddress"
      | "tokenAddress"
      | "transferOwnership"
  ): FunctionFragment;

  encodeFunctionData(
    functionFragment: "_mappingPrice",
    values: [BigNumberish]
  ): string;
  encodeFunctionData(
    functionFragment: "_marketIdtoId",
    values: [BigNumberish]
  ): string;
  encodeFunctionData(
    functionFragment: "_owner",
    values: [BigNumberish]
  ): string;
  encodeFunctionData(
    functionFragment: "buyItem",
    values: [BigNumberish]
  ): string;
  encodeFunctionData(
    functionFragment: "delistItem",
    values: [BigNumberish]
  ): string;
  encodeFunctionData(
    functionFragment: "getListedItem",
    values?: undefined
  ): string;
  encodeFunctionData(
    functionFragment: "getMyListedItem",
    values?: undefined
  ): string;
  encodeFunctionData(
    functionFragment: "listBatchItem",
    values: [BigNumberish[], BigNumberish[]]
  ): string;
  encodeFunctionData(
    functionFragment: "listItem",
    values: [BigNumberish, BigNumberish]
  ): string;
  encodeFunctionData(
    functionFragment: "nftAddress",
    values?: undefined
  ): string;
  encodeFunctionData(
    functionFragment: "onERC721Received",
    values: [string, string, BigNumberish, BytesLike]
  ): string;
  encodeFunctionData(functionFragment: "owner", values?: undefined): string;
  encodeFunctionData(
    functionFragment: "renounceOwnership",
    values?: undefined
  ): string;
  encodeFunctionData(
    functionFragment: "setNftAddress",
    values: [string]
  ): string;
  encodeFunctionData(
    functionFragment: "setTokenAddress",
    values: [string]
  ): string;
  encodeFunctionData(
    functionFragment: "tokenAddress",
    values?: undefined
  ): string;
  encodeFunctionData(
    functionFragment: "transferOwnership",
    values: [string]
  ): string;

  decodeFunctionResult(
    functionFragment: "_mappingPrice",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "_marketIdtoId",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "_owner", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "buyItem", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "delistItem", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "getListedItem",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "getMyListedItem",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "listBatchItem",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "listItem", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "nftAddress", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "onERC721Received",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "owner", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "renounceOwnership",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "setNftAddress",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "setTokenAddress",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "tokenAddress",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "transferOwnership",
    data: BytesLike
  ): Result;

  events: {
    "BuyItem(address,uint256)": EventFragment;
    "DelistItem(address,uint256)": EventFragment;
    "ListItem(address,uint256,uint256)": EventFragment;
    "OwnershipTransferred(address,address)": EventFragment;
  };

  getEvent(nameOrSignatureOrTopic: "BuyItem"): EventFragment;
  getEvent(nameOrSignatureOrTopic: "DelistItem"): EventFragment;
  getEvent(nameOrSignatureOrTopic: "ListItem"): EventFragment;
  getEvent(nameOrSignatureOrTopic: "OwnershipTransferred"): EventFragment;
}

export interface BuyItemEventObject {
  buyer: string;
  itemId: BigNumber;
}
export type BuyItemEvent = TypedEvent<[string, BigNumber], BuyItemEventObject>;

export type BuyItemEventFilter = TypedEventFilter<BuyItemEvent>;

export interface DelistItemEventObject {
  owner: string;
  itemId: BigNumber;
}
export type DelistItemEvent = TypedEvent<
  [string, BigNumber],
  DelistItemEventObject
>;

export type DelistItemEventFilter = TypedEventFilter<DelistItemEvent>;

export interface ListItemEventObject {
  lister: string;
  itemId: BigNumber;
  price: BigNumber;
}
export type ListItemEvent = TypedEvent<
  [string, BigNumber, BigNumber],
  ListItemEventObject
>;

export type ListItemEventFilter = TypedEventFilter<ListItemEvent>;

export interface OwnershipTransferredEventObject {
  previousOwner: string;
  newOwner: string;
}
export type OwnershipTransferredEvent = TypedEvent<
  [string, string],
  OwnershipTransferredEventObject
>;

export type OwnershipTransferredEventFilter =
  TypedEventFilter<OwnershipTransferredEvent>;

export interface Marketplace extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this;
  attach(addressOrName: string): this;
  deployed(): Promise<this>;

  interface: MarketplaceInterface;

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TEvent>>;

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>
  ): Array<TypedListener<TEvent>>;
  listeners(eventName?: string): Array<Listener>;
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>
  ): this;
  removeAllListeners(eventName?: string): this;
  off: OnEvent<this>;
  on: OnEvent<this>;
  once: OnEvent<this>;
  removeListener: OnEvent<this>;

  functions: {
    _mappingPrice(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<[BigNumber]>;

    _marketIdtoId(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<[BigNumber]>;

    _owner(arg0: BigNumberish, overrides?: CallOverrides): Promise<[string]>;

    buyItem(
      _marketId: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    delistItem(
      _marketId: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    getListedItem(overrides?: CallOverrides): Promise<[BigNumber[]]>;

    getMyListedItem(overrides?: CallOverrides): Promise<[BigNumber[]]>;

    listBatchItem(
      _itemIds: BigNumberish[],
      _prices: BigNumberish[],
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    listItem(
      _itemId: BigNumberish,
      _price: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    nftAddress(overrides?: CallOverrides): Promise<[string]>;

    onERC721Received(
      operator: string,
      from: string,
      tokenId: BigNumberish,
      data: BytesLike,
      overrides?: CallOverrides
    ): Promise<[string]>;

    owner(overrides?: CallOverrides): Promise<[string]>;

    renounceOwnership(
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setNftAddress(
      _nftAddress: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setTokenAddress(
      _tokenAddress: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    tokenAddress(overrides?: CallOverrides): Promise<[string]>;

    transferOwnership(
      newOwner: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;
  };

  _mappingPrice(
    arg0: BigNumberish,
    overrides?: CallOverrides
  ): Promise<BigNumber>;

  _marketIdtoId(
    arg0: BigNumberish,
    overrides?: CallOverrides
  ): Promise<BigNumber>;

  _owner(arg0: BigNumberish, overrides?: CallOverrides): Promise<string>;

  buyItem(
    _marketId: BigNumberish,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  delistItem(
    _marketId: BigNumberish,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  getListedItem(overrides?: CallOverrides): Promise<BigNumber[]>;

  getMyListedItem(overrides?: CallOverrides): Promise<BigNumber[]>;

  listBatchItem(
    _itemIds: BigNumberish[],
    _prices: BigNumberish[],
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  listItem(
    _itemId: BigNumberish,
    _price: BigNumberish,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  nftAddress(overrides?: CallOverrides): Promise<string>;

  onERC721Received(
    operator: string,
    from: string,
    tokenId: BigNumberish,
    data: BytesLike,
    overrides?: CallOverrides
  ): Promise<string>;

  owner(overrides?: CallOverrides): Promise<string>;

  renounceOwnership(
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  setNftAddress(
    _nftAddress: string,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  setTokenAddress(
    _tokenAddress: string,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  tokenAddress(overrides?: CallOverrides): Promise<string>;

  transferOwnership(
    newOwner: string,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  callStatic: {
    _mappingPrice(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    _marketIdtoId(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    _owner(arg0: BigNumberish, overrides?: CallOverrides): Promise<string>;

    buyItem(_marketId: BigNumberish, overrides?: CallOverrides): Promise<void>;

    delistItem(
      _marketId: BigNumberish,
      overrides?: CallOverrides
    ): Promise<void>;

    getListedItem(overrides?: CallOverrides): Promise<BigNumber[]>;

    getMyListedItem(overrides?: CallOverrides): Promise<BigNumber[]>;

    listBatchItem(
      _itemIds: BigNumberish[],
      _prices: BigNumberish[],
      overrides?: CallOverrides
    ): Promise<void>;

    listItem(
      _itemId: BigNumberish,
      _price: BigNumberish,
      overrides?: CallOverrides
    ): Promise<void>;

    nftAddress(overrides?: CallOverrides): Promise<string>;

    onERC721Received(
      operator: string,
      from: string,
      tokenId: BigNumberish,
      data: BytesLike,
      overrides?: CallOverrides
    ): Promise<string>;

    owner(overrides?: CallOverrides): Promise<string>;

    renounceOwnership(overrides?: CallOverrides): Promise<void>;

    setNftAddress(
      _nftAddress: string,
      overrides?: CallOverrides
    ): Promise<void>;

    setTokenAddress(
      _tokenAddress: string,
      overrides?: CallOverrides
    ): Promise<void>;

    tokenAddress(overrides?: CallOverrides): Promise<string>;

    transferOwnership(
      newOwner: string,
      overrides?: CallOverrides
    ): Promise<void>;
  };

  filters: {
    "BuyItem(address,uint256)"(buyer?: null, itemId?: null): BuyItemEventFilter;
    BuyItem(buyer?: null, itemId?: null): BuyItemEventFilter;

    "DelistItem(address,uint256)"(
      owner?: null,
      itemId?: null
    ): DelistItemEventFilter;
    DelistItem(owner?: null, itemId?: null): DelistItemEventFilter;

    "ListItem(address,uint256,uint256)"(
      lister?: null,
      itemId?: null,
      price?: null
    ): ListItemEventFilter;
    ListItem(lister?: null, itemId?: null, price?: null): ListItemEventFilter;

    "OwnershipTransferred(address,address)"(
      previousOwner?: string | null,
      newOwner?: string | null
    ): OwnershipTransferredEventFilter;
    OwnershipTransferred(
      previousOwner?: string | null,
      newOwner?: string | null
    ): OwnershipTransferredEventFilter;
  };

  estimateGas: {
    _mappingPrice(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    _marketIdtoId(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    _owner(arg0: BigNumberish, overrides?: CallOverrides): Promise<BigNumber>;

    buyItem(
      _marketId: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    delistItem(
      _marketId: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    getListedItem(overrides?: CallOverrides): Promise<BigNumber>;

    getMyListedItem(overrides?: CallOverrides): Promise<BigNumber>;

    listBatchItem(
      _itemIds: BigNumberish[],
      _prices: BigNumberish[],
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    listItem(
      _itemId: BigNumberish,
      _price: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    nftAddress(overrides?: CallOverrides): Promise<BigNumber>;

    onERC721Received(
      operator: string,
      from: string,
      tokenId: BigNumberish,
      data: BytesLike,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    owner(overrides?: CallOverrides): Promise<BigNumber>;

    renounceOwnership(
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    setNftAddress(
      _nftAddress: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    setTokenAddress(
      _tokenAddress: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    tokenAddress(overrides?: CallOverrides): Promise<BigNumber>;

    transferOwnership(
      newOwner: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;
  };

  populateTransaction: {
    _mappingPrice(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    _marketIdtoId(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    _owner(
      arg0: BigNumberish,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    buyItem(
      _marketId: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    delistItem(
      _marketId: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    getListedItem(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    getMyListedItem(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    listBatchItem(
      _itemIds: BigNumberish[],
      _prices: BigNumberish[],
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    listItem(
      _itemId: BigNumberish,
      _price: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    nftAddress(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    onERC721Received(
      operator: string,
      from: string,
      tokenId: BigNumberish,
      data: BytesLike,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    owner(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    renounceOwnership(
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    setNftAddress(
      _nftAddress: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    setTokenAddress(
      _tokenAddress: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    tokenAddress(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    transferOwnership(
      newOwner: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;
  };
}
